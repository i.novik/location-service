package com.novik.locationservice.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class CompanySummary {
    private Long id;
    private String name;
    private String status;
    private String country;
    private String vatNumber;
    private String currencyIso;
}
