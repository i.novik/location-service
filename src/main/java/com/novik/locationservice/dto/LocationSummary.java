package com.novik.locationservice.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class LocationSummary {
    private Long id;
    private String name;
    private String address;
    private String country;
    private String city;
    private Long companyId;
}
