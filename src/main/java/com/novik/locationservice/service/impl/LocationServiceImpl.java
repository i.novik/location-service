package com.novik.locationservice.service.impl;

import com.novik.locationservice.dto.LocationSummary;
import com.novik.locationservice.mapper.LocationToLocationSummaryMapper;
import com.novik.locationservice.model.Location;
import com.novik.locationservice.repository.LocationRepository;
import com.novik.locationservice.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LocationServiceImpl implements LocationService {

    private final LocationRepository locationRepository;
    private final LocationToLocationSummaryMapper locationToLocationSummaryMapper;

    @Override
    public LocationSummary getLocation(long id) {
        return locationToLocationSummaryMapper.map(locationRepository.getOne(id));
    }

    @Override
    public Long getCompanyId(long locationId) {
        Location location = locationRepository.getOne(locationId);
        return location.getCompanyId();
    }

    @Override
    public List<LocationSummary> getCompanyLocations(long companyId) {
        List<Location> locations = locationRepository.findAllByCompanyId(companyId);
        return locationToLocationSummaryMapper.map(locations);
    }
}
