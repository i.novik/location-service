package com.novik.locationservice.service;

import com.novik.locationservice.dto.LocationSummary;

import java.util.List;

public interface LocationService {
    LocationSummary getLocation(long id);

    Long getCompanyId(long locationId);

    List<LocationSummary> getCompanyLocations(long companyId);
}
