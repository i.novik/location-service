package com.novik.locationservice.controller;

import com.novik.locationservice.dto.CompanySummary;
import com.novik.locationservice.dto.LocationSummary;
import com.novik.locationservice.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/locations/", produces = MediaType.APPLICATION_JSON_VALUE)
public class LocationController {

    private final LocationService locationService;
    private final RestTemplate restTemplate;

    @Value("${services.company.api}")
    private String companyServiceApi;

    @GetMapping("/ping")
    public ResponseEntity getPing() {
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/{id}")
    public LocationSummary getLocation(@PathVariable final long id) {
        return locationService.getLocation(id);
    }

    @GetMapping("/company/{id}")
    public List<LocationSummary> getCompanyLocations(@PathVariable final long id) {
        return locationService.getCompanyLocations(id);
    }

    @GetMapping("/{id}/suppliers")
    public CompanySummary getSuppliers(@PathVariable final long id) {
        Long companyId = locationService.getCompanyId(id);
        Map<String, Long> params = new HashMap<>();
        params.put("id", companyId);

        return restTemplate.getForObject(companyServiceApi + "/{id}", CompanySummary.class, params);
    }
}
