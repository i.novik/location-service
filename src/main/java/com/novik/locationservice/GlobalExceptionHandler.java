package com.novik.locationservice;

import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class GlobalExceptionHandler {
  private Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  @ExceptionHandler({
      IllegalArgumentException.class,
      PropertyReferenceException.class,
      HttpMessageNotReadableException.class
  })
  public ResponseEntity illegalArgumentInRequest(RuntimeException exc) {
    log.error("Bad request:", exc);

    return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body("Bad request occurred.");
  }

  @ExceptionHandler({
      NumberFormatException.class,
      NotFoundException.class
  })
  public ResponseEntity notFoundException(RuntimeException exc) {
    log.error("resourceNotFoundException:", exc);

    return ResponseEntity
        .status(HttpStatus.NOT_FOUND)
        .body("Resource not found.");
  }

  @ExceptionHandler({
      ConstraintViolationException.class,
  })
  public ResponseEntity dataIntegrityException(ConstraintViolationException exc) {
    log.error("constraintViolationException:", exc);

    return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body("Data rules failed.");
  }

  @ExceptionHandler(Throwable.class)
  public ResponseEntity genericException(Throwable thr) {
    log.error("genericException:", thr);

    return ResponseEntity
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body("Generic exception occurred - contact service administrator.");
  }

}
