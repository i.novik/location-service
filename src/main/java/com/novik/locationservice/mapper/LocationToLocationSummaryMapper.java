package com.novik.locationservice.mapper;

import com.novik.locationservice.dto.LocationSummary;
import com.novik.locationservice.model.Location;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class LocationToLocationSummaryMapper {

    public LocationSummary map(Location location) {
        return LocationSummary.builder()
                .id(location.getId())
                .name(location.getName())
                .address(location.getAddress())
                .city(location.getCity())
                .country(location.getCountry())
                .companyId(location.getCompanyId())
                .build();
    }

    public List<LocationSummary> map(List<Location> locations) {
        return locations.stream()
                .map(this::map)
                .collect(Collectors.toList());
    }
}
