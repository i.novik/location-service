package com.novik.locationservice.service.impl;

import com.novik.locationservice.dto.LocationSummary;
import com.novik.locationservice.mapper.LocationToLocationSummaryMapper;
import com.novik.locationservice.model.Location;
import com.novik.locationservice.repository.LocationRepository;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.NotNull;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class LocationServiceImplTest {

    @Mock
    private LocationRepository locationRepository;

    @Mock
    private LocationToLocationSummaryMapper locationMapper;

    @InjectMocks
    private LocationServiceImpl locationServiceImpl;

    @Test
    public void whenGetLocationById_thenReturnLocationSummary() {
        //Given
        Location testLocation = new Location(
                1L,
                "testName",
                "testAdress",
                "testCountry",
                "testCity",
                1L);
        when(this.locationRepository.getOne(1L)).thenReturn(testLocation);
        when(this.locationMapper.map(testLocation)).thenReturn(
                LocationSummary.builder()
                        .id(testLocation.getId())
                        .name(testLocation.getName())
                        .address(testLocation.getAddress())
                        .country(testLocation.getCountry())
                        .city(testLocation.getCity())
                        .companyId(testLocation.getCompanyId())
                        .build());

        //When
        LocationSummary locationSummary = locationServiceImpl.getLocation(1L);

        //Then
        assertLocationSummary(testLocation, locationSummary);
    }

    @Test
    public void whenGetCompanyId_thenReturnCompanyId() {
        //Given
        Long givenLocationId = 1L;
        Long givenCompanyId = 3L;
        Location location = Location.builder()
                .companyId(givenCompanyId)
                .build();
        when(this.locationRepository.getOne(givenLocationId)).thenReturn(location);

        //When
        Long companyId = locationServiceImpl.getCompanyId(givenLocationId);

        //Then
        assertEquals(givenCompanyId, companyId);
    }

    @Test
    public void whenGetCompanyLocations_thenReturnAllFoundedByCompanyIdLocations() {
        //Given
        Location testLocation = new Location(
                1L,
                "testName",
                "testAdress",
                "testCountry",
                "testCity",
                1L);
        Location secondTestLocation = new Location(
                2L,
                "testName2",
                "testAdress2",
                "testCountry2",
                "testCity2",
                1L);

        List<Location> locations = asList(testLocation, secondTestLocation);

        when(this.locationRepository.findAllByCompanyId(1L)).thenReturn(locations);
        when(this.locationMapper.map(locations)).thenReturn(
                asList(
                        LocationSummary.builder()
                                .id(testLocation.getId())
                                .name(testLocation.getName())
                                .address(testLocation.getAddress())
                                .country(testLocation.getCountry())
                                .city(testLocation.getCity())
                                .companyId(testLocation.getCompanyId())
                                .build(),
                        LocationSummary.builder()
                                .id(secondTestLocation.getId())
                                .name(secondTestLocation.getName())
                                .address(secondTestLocation.getAddress())
                                .country(secondTestLocation.getCountry())
                                .city(secondTestLocation.getCity())
                                .companyId(secondTestLocation.getCompanyId())
                                .build()));

        //When
        List<LocationSummary> locationSummaries = locationServiceImpl.getCompanyLocations(1L);

        //Then
        assertLocationSummary(testLocation, locationSummaries.stream()
                .filter(summary -> summary.getId().equals(testLocation.getId()))
                .findFirst()
                .get());
        assertLocationSummary(secondTestLocation, locationSummaries.stream()
                .filter(summary -> summary.getId().equals(secondTestLocation.getId()))
                .findFirst()
                .get());
    }

    private void assertLocationSummary(@NotNull Location expected, @NotNull LocationSummary actual) {
        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(actual.getId()).isEqualTo(expected.getId());
        softAssertions.assertThat(actual.getName()).isEqualTo(expected.getName());
        softAssertions.assertThat(actual.getAddress()).isEqualTo(expected.getAddress());
        softAssertions.assertThat(actual.getCountry()).isEqualTo(expected.getCountry());
        softAssertions.assertThat(actual.getCity()).isEqualTo(expected.getCity());
        softAssertions.assertThat(actual.getCompanyId()).isEqualTo(expected.getCompanyId());
    }
}