package com.novik.locationservice.controllers;

import com.novik.locationservice.controller.LocationController;
import com.novik.locationservice.dto.CompanySummary;
import com.novik.locationservice.dto.LocationSummary;
import com.novik.locationservice.service.LocationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LocationController.class)
public class LocationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LocationService locationService;

    @MockBean
    private RestTemplate restTemplate;

    @Value("${services.company.api}")
    private String companyServiceApi;

    @Test
    public void getPing() throws Exception {
        this.mockMvc.perform(get("/api/locations/ping")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void getLocation() throws Exception {

        LocationSummary locationSummary = LocationSummary.builder()
                .id(1L)
                .name("Google inc.")
                .address("Victory av., 5")
                .country("Belarus")
                .city("Minsk")
                .companyId(2L)
                .build();
        given(this.locationService.getLocation(1))
                .willReturn(locationSummary);
        this.mockMvc.perform(get("/api/locations/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().json(
                        "{\"id\":" + locationSummary.getId() + "," +
                                "\"name\":\"" + locationSummary.getName() + "\"," +
                                "\"address\":\"" + locationSummary.getAddress() + "\"," +
                                "\"country\":\"" + locationSummary.getCountry() + "\"," +
                                "\"city\":\"" + locationSummary.getCity() + "\"," +
                                "\"companyId\":" + locationSummary.getCompanyId() + "}"));
    }

    @Test
    public void getCompanyLocations() throws Exception {

        List<LocationSummary> locationSummaries = asList(
                LocationSummary.builder()
                        .id(1L)
                        .name("Google inc.")
                        .address("Victory av., 5")
                        .country("Belarus")
                        .city("Minsk")
                        .companyId(2L)
                        .build(),
                LocationSummary.builder()
                        .id(2L)
                        .name("Google inc.")
                        .address("Victory av., 5")
                        .country("Belarus")
                        .city("Minsk")
                        .companyId(2L)
                        .build());
        given(this.locationService.getCompanyLocations(2L))
                .willReturn(locationSummaries);
        this.mockMvc.perform(get("/api/locations/company/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    public void getSupplier() throws Exception {
        LocationSummary locationSummary = LocationSummary.builder()
                .id(1L)
                .companyId(2L)
                .build();
        CompanySummary companySummary = CompanySummary.builder()
                .id(2L)
                .name("Google inc.")
                .status("Active")
                .country("Belarus")
                .vatNumber("10EW1")
                .currencyIso("BYR")
                .build();
        Map<String, Long> params = new HashMap<>();
        params.put("id", locationSummary.getCompanyId());

        given(this.locationService.getCompanyId(locationSummary.getId()))
                .willReturn(locationSummary.getCompanyId());
        String url = companyServiceApi + "/{id}";
        given(this.restTemplate.getForObject(url, CompanySummary.class, params))
                .willReturn(companySummary);

        this.mockMvc.perform(get("/api/locations/1/suppliers"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\"id\":" + companySummary.getId() + "," +
                                "\"name\":\"" + companySummary.getName() + "\"," +
                                "\"status\":\"" + companySummary.getStatus() + "\"," +
                                "\"country\":\"" + companySummary.getCountry() + "\"," +
                                "\"vatNumber\":\"" + companySummary.getVatNumber() + "\"," +
                                "\"currencyIso\":" + companySummary.getCurrencyIso() + "}"));
    }
}