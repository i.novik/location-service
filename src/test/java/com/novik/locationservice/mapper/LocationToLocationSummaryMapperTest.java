package com.novik.locationservice.mapper;

import com.novik.locationservice.dto.LocationSummary;
import com.novik.locationservice.model.Location;
import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class LocationToLocationSummaryMapperTest {
  private LocationToLocationSummaryMapper locationMapper;

  @Before
  public void setup() {
    locationMapper = new LocationToLocationSummaryMapper();
  }

  @Test
  public void testMap_basic() {
    // Given
    Location testLocation = new Location(
        1L,
        "testName",
        "testAdress",
        "testCountry",
        "testCity",
        1L);

    // When
    LocationSummary locationSummary = locationMapper.map(testLocation);

    //Then
    SoftAssertions softAssertions = new SoftAssertions();
    softAssertions.assertThat(locationSummary.getId()).isEqualTo(testLocation.getId());
    softAssertions.assertThat(locationSummary.getName()).isEqualTo(testLocation.getName());
    softAssertions.assertThat(locationSummary.getAddress()).isEqualTo(testLocation.getAddress());
    softAssertions.assertThat(locationSummary.getCountry()).isEqualTo(testLocation.getCountry());
    softAssertions.assertThat(locationSummary.getCity()).isEqualTo(testLocation.getCity());
    softAssertions.assertThat(locationSummary.getCompanyId()).isEqualTo(testLocation.getCompanyId());
  }
}
