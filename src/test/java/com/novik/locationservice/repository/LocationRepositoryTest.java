package com.novik.locationservice.repository;

import com.novik.locationservice.model.Location;
import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.NotNull;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class LocationRepositoryTest {
    @Autowired
    private LocationRepository locationRepository;

    private Location testLocation;

    @Before
    public void prepareAndSaveTestLocation() {
        testLocation = locationRepository.saveAndFlush(new Location(
                null,
                "testName",
                "testAdress",
                "testCountry",
                "testCity",
                2L));
    }

    @Test
    public void whenGetOneById_thenReturnLocation() {
        // given

        //When
        Location savedLocation = locationRepository.getOne(testLocation.getId());

        //Then
        assertLocations(savedLocation, testLocation);
    }

    @Test
    public void whenGetCompanyId_thenReturnLocationCompanyId() {
        // given

        //When
        Long companyId = locationRepository.getCompanyId(testLocation.getId());

        //Then
        assertEquals(testLocation.getCompanyId(), companyId);
    }

    @Test
    public void whenGetLocationByCompanyID_thenReturnLocation() {
        // given
        Location secondTestLocation = locationRepository.saveAndFlush(new Location(
                null,
                "testName2",
                "testAdress2",
                "testCountry2",
                "testCity2",
                2L));

        //When
        List<Location> foundedLocations = locationRepository.findAllByCompanyId(testLocation.getCompanyId());

        //Then
        assertTrue(foundedLocations.contains(testLocation));
        assertTrue(foundedLocations.contains(secondTestLocation));
    }

    private void assertLocations(@NotNull Location expected, @NotNull Location actual) {
        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(expected.getId()).isEqualTo(actual.getId());
        softAssertions.assertThat(expected.getName()).isEqualTo(actual.getName());
        softAssertions.assertThat(expected.getAddress()).isEqualTo(actual.getAddress());
        softAssertions.assertThat(expected.getCountry()).isEqualTo(actual.getCountry());
        softAssertions.assertThat(expected.getCity()).isEqualTo(actual.getCity());
    }
}